import java.util.ArrayList;
import java.util.Random;

public class lab_work {
   public static void main(String []args) {
      
      ArrayList<Integer> arr = new ArrayList<>();
      
      Random randomGenerator = new Random();

      for (int idx = 0; idx < 10; ++idx) {
	int randomInt = randomGenerator.nextInt(100);
      	arr.add(randomInt);
    }
      System.out.println("ArrayList elements are: " + arr);

      int max = arr.get(0);
      int min = arr.get(0);

      for (int x = 1; x < 10; ++x) {
	if (max < arr.get(x))
		max = arr.get(x);
	if (min > arr.get(x))
		min = arr.get(x);
	}

      System.out.println("Minimum: " + min);
      System.out.println("Maximum: " + max);

   }
} 
